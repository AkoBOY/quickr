<?php
session_start();
// Подключение библиотек
require('includes/date.php');
require('includes/news.php');
require_once("includes/connect.php");
    /*Проверка полей авторизации*/

    if(isset($_POST["login"])){
      if(!empty($_POST['username']) && !empty($_POST['password'])) {
      $username=htmlspecialchars($_POST['username']);
      $password=htmlspecialchars($_POST['password']);
      $query_avto = mysql_query("SELECT * FROM `t_users` WHERE `email` ='$username' AND `password` ='$password'");
      $numrows=mysql_num_rows($query_avto);
    if($numrows!=0)
        {
        	/*Добавление в кеш*/
          $_SESSION['session_username'] =$username; 

          /* Перенаправление браузера */
          echo "<META HTTP-EQUIV='REFRESH' CONTENT='0;URL= authorization.php'> ";
  } 
      else {
              $message = "Неправильный пароль или логин";
            }
  } 
}
  ?>
<html>
<head>
	<title>QuickR-Отчет по НИИ</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
	<!-- style -->
	<link rel="shortcut icon" href="img/favicon.png">
	<link rel="stylesheet" href="fi/flaticon.css">
	<link rel="stylesheet" href="css/font-awesome.css">
	<link rel="stylesheet" href="css/main.css">
</head>
<body class="pc">
    <header class="only-color">
		<div class="sticky-wrapper">
			<div class="sticky-menu">
				<div class="grid-row clear-fix">
					<a href="index.php" class="logo">
						<img src="img/logo.png">
						<h1>QuickR</h1>
					</a>
					<nav class="main-nav">

						<i class="mobile_menu_switcher"></i>
						<ul class="clear-fix">
							<li id="menu-item-0">
								<a href="index.php">Поиск</a>
							</li>
							<li id="menu-item-3">
								<a href="news.php" class="active">Новости</a>
							</li>
              <?php
                    if(isset($_SESSION["session_username"])){
              ?>
              <li id="menu-item-2">
								<a href="authorization.php" class="active">Личный кабинет</a>
								<div class="login-block">
									<form class="login-form">
									<a href="logout.php">Выйти</a>
									</form>
								</div>
							</li>
              <?php
              }else{
              ?>
							<li id="menu-item-2">
								<a>Войти</a>
								<div class="login-block">

									<h2>Авторизация</h2>
									<form class="login-form" method="post">
										<div class="form-group">
											<input type="text" class="login-input" placeholder="E-mail" name="username">
											<span class="input-icon">
												<i class="fa fa-user"></i>
											</span>
										</div>
										<div class="form-group">
											<input type="password" class="login-input" name = 'password' onblur="if(this.value=='')this.value='Пароль'" onfocus="if(this.value=='Пароль')this.value='' " placeholder="Пароль">
											<span class="input-icon">
												<i class="fa fa-lock"></i>
											</span>
										</div>
										<p class="small">
											<a href="">Забыли пароль?</a>
										</p>
										<button name = 'login' class="btn_log cws-button bt-color-5 border-radius">Войти</button>
										<?php
											echo $message;
										?>
									</form>
								</div>
							</li>
							<li id="menu-item-3">
								<a href="login.php">Зарегистрироваться</a>
							</li>
              <?php
                }
              ?>
						</ul>
					</nav>
				</div>
			</div>
		</div>
    </header>

    <div class="page-content container clear-fix">

        <div class="grid-col-row">

            <div class="grid-col grid-col-9">
                <main>
                    <div class="blog-post">
                        <article>
                            <div class="post-info clear-fix">
                                <div class="date-post"><div class="day"><?php echo $day; ?></div><div class="month"><?php echo $mes;?></div></div>
                                <div class="post-info-main">
                                    <div class="news">Новые события</div>
                                </div>
                            </div>
                            <aside class="widget-event">
								<h2>События за месяц</h2>
								<?php
									news();
								?>
							</aside>
                    </div>
                </main>
            </div>
            <div class="grid-col grid-col-3 sidebar">
                 <aside class="widget-comments">
                    <h2>Рейтинг авторов</h2>
                    <hr class="divider-big" />
						<h2>ТОП-10</h2>
					<hr class="divider-big" />
                    <div class="comments">
                        <?php
							news_top();
						?>
                    </div>
                </aside>
            </div>
        </div>
    </div>

    <footer>
        <div class="footer-bottom">
            <div class="grid-row clear-fix">
                <div class="copyright">QuickR<span></span> 2017 г. Разработчик: Декун Надежда, НМТ-443907.</div>
                <nav class="footer-nav">
                    <ul class="clear-fix">
                        <li>
                            <a href="index.html">Поиск</a>
                        </li>
                        <li>
                            <a href="news.html">Новости</a>
                        </li>
                        <li>
                            <a href="login.html">Зарегистрироваться</a>
                        </li>
                        <li>
                            <a href="">Войти</a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    </footer>
</body>
</html>
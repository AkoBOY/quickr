<?php
require('connect.php');

function publicationData(){
	$data = [];
	/*
	 * USER
	 */
	$userQuery = 'SELECT * FROM `t_users` where `email` = "'.$_SESSION['session_username'].'" ';
	$userResult = mysql_query($userQuery);
	if(!$userResult) {
		echo '<pre>';
			echo "Ошибка при выполнении запроса: ($userQuery) from DB: " . mysql_error();
		echo '</pre>';
		exit;
	}
	$user = [];
	while ($row = mysql_fetch_assoc($userResult)) {
		$user = [
			'user_id' => $row['user_id'],
			'lastname' => $row['lastname'],
			'firstname' => $row['firstname'],
			'secondname' => $row['secondname'],
			'date' => $row['date'],
			'sex' => $row['sex'],
			'inst_id' => $row['inst_id'],
			'post_id' => $row['post_id'],
			'academ_title_id' => $row['academ_title_id'],
			'degree_id' => $row['degree_id'],
			'phone' => $row['phone'],
			'email' => $row['email'],
			'password' => $row['password'],
			'reg_ip' => $row['reg_ip']
		];
	}
	$user['fullname'] = $user['lastname'] . ' ' . $user['firstname'] . ' ' . $user['secondname'];

	/*
	 * Publication
	 */
	$publicationQuery = 'SELECT * FROM `t_sprav_publication` where `name_author` = "'.$user['fullname'].'" ';
	$publicationResult = mysql_query($publicationQuery);
	if(!$publicationResult) {
		echo '<pre>';
		echo "Ошибка при выполнении запроса: ($publicationQuery) from DB: " . mysql_error();
		echo '</pre>';
		exit;
	}
	while ($row = mysql_fetch_assoc($publicationResult)) {
		$data[$row['public_id']] = [
			'public_id' => $row['public_id'],
			'name_public' => $row['name_public'],
			'type_public' => $row['type_public'],
			'year_public' => $row['year_public'],
			'name_author' => $row['name_author'],
			'name_avtor_1' => $row['name_avtor_1'],
			'name_avtor_2' => $row['name_avtor_2'],
			'name_avtor_3' => $row['name_avtor_3'],
			'conf_id' => $row['conf_id'],
			'num_public' => $row['num_public'],
			'date_registr' => $row['date_registr'],
			'journal_id' => $row['journal_id'],
			'num_tome' => $row['num_tome'],
			'num_journal' => $row['num_journal'],
			'num_DOI' => $row['num_DOI'],
			'izdat' => $row['izdat'],
			'user' => $row['user']
		];
	}
	return $data;
}

function publicationRender($data) {
	$html = '<table class="result_search_login">
			<tr>
				<td>№</td>
				<td>Название научного труда</td>
				<td>Год публикации</td>
				<td>Тип публикации</td>
				<td>Издательство</td>
				<td>Объем</td>
				<td>Автор</td>
				<td>Изменить</td>
				<td>Добавить в отчет</td>
			</tr>';

	foreach ( $data as $key => $value ) {
		$i ++;
		$html .= "
			<tr>
				<td>" . $i . "</td>
				<td>" . $value['name_public'] . "</td>
				<td>" . $value['year_public'] . "</td>
				<td>" . $value['type_public'] . "</td>
				<td>" . $value['izdat'] . "</td>
				<td>" . $value['num_public'] . "</td>
				<td>" . $value['name_author'] . "</td>
				<td class='print'>
					<a href='index.html' class='fa fa-pencil fa-2x' OnClick='OnSubmitForm();' title='Изменить'></a>
				</td>
				<td>
					<div class='checkbox_search'>
						<input type='checkbox' id='checkbox11' name='type_article'/>
						<label for='checkbox11'></label>
					</div>
				</td>
			</tr>
			";
	}
	$html .= "</table>";

	return $html;
}

function renderPdf($data) {
	$html = '<!doctype html>
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <style>
        body {
            font-size: 12px;
        }
        table {
            border-collapse: collapse;
            width: 96%;
            margin: 2%;
        }
        table thead {
            text-align: center;
            font-weight: bold;
        }
        table td {
            border: 1px solid #000;
            padding: 10px;
        }
        td:nth-child(1),
        td:nth-child(6) {
            width: 5%;
        }

        td:nth-child(3),
        td:nth-child(4) {
            width: 10%;
        }
        td:nth-child(7) {
            width:10%;
        }
        td:nth-child(2),
        td:nth-child(5) {
            width: 30%;
        }
        /* 5-60: 2-40*/
    </style>
</head>
<body>
<h1>Title какой нибудь</h1>
<table>
    <thead>
    <tr>
        <td>№</td>
        <td>Название научного труда</td>
        <td>Год публикации</td>
        <td>Тип публикации</td>
        <td>Издательство</td>
        <td>Объем</td>
        <td>Автор</td>
    </tr>
    </thead>';

	foreach ( $data as $key => $value ) {
		$i ++;
		$html .= "
			<tr>
				<td>" . $i . "</td>
				<td>" . $value['name_public'] . "</td>
				<td>" . $value['year_public'] . "</td>
				<td>" . $value['type_public'] . "</td>
				<td>" . $value['izdat'] . "</td>
				<td>" . $value['num_public'] . "</td>
				<td>" . $value['name_author'] . "</td>
			</tr>
			";
	}
		$html .= '
			</table>
			</body>
			</html>';
		return $html;
}
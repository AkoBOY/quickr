<?php
session_start();
    require('includes/connect.php');
	require('includes/info_user.php');
	require('includes/new_publication.php');
	require('includes/publication.php');
?>
<html>
<head>
    <title>QuickR-Отчет по НИИ</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
    <!-- style -->
    <link rel="shortcut icon" href="img/favicon.png">
    <link rel="stylesheet" href="fi/flaticon.css">
    <link rel="stylesheet" href="css/font-awesome.css">
    <link rel="stylesheet" href="css/main.css">
    <style>
        #menu-item-2:hover .login-block {
            display: block !important;
        }
    </style>
    <script src="js/jquery.min.js"></script>
</head>
<body class="pc">
	<header class="only-color">
		<div class="sticky-wrapper">
			<div class="sticky-menu">
				<div class="grid-row clear-fix">
					<a href="index.php" class="logo">
						<img src="img/logo.png">
						<h1>QuickR</h1>
					</a>
					<nav class="main-nav">
						<i class="mobile_menu_switcher"></i>
						<ul class="clear-fix">
							<li id="menu-item-0">
								<a href="index.php">Поиск</a>
							</li>
							<li id="menu-item-3">
								<a href="news.php">Новости</a>
							</li>
							<li id="menu-item-3">
								<li id="menu-item-2">
								<a href="authorization.php" class="active">Личный кабинет</a>
								<div class="login-block">
									<form class="login-form">
										<a href="logout.php">Выйти</a>
									</form>
								</div>
							</li>
							</li>
						</ul>
					</nav>
				</div>
			</div>
		</div>
	</header>
	<section>
		<div align="center" class="clear-fix">
			<div class="grid-col-6">
				<div class="banner-offer icon-right bg-color-4">
					<div class="banner-text">Личный кабинет</div>
					<div class="banner-icon">
						<i class="fa fa-user"></i>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section style="margin-top:25px">
		<div class="tabs">
			<div class="block-tabs-btn clear-fix">

				<div class="tabs-btn" data-tabs-id="tabs1">Основные данные</div>
				<div class="tabs-btn active" data-tabs-id="tabs2">Добавить</div>
				<div class="tabs-btn " data-tabs-id="tabs3">Список работ</div>
				<div class="tabs-btn" data-tabs-id="tabs4">Поиск</div>
			</div>
			<div class="tabs-keeper">

				<div class="container-tabs" data-tabs-id="cont-tabs1">
				<form method="post">
					<div class="container clear-fix">
						<table class="add_users">
							<tr>
								<td>
									<div class="form-row add_users ">
										<h4>Фамилия</h4>
										<input type="text" class="add_users" value="" placeholder= "<?php echo $lastname; ?>" name="lastname" required disabled >
									</div>
								</td>
								<td>
									<div class="form-row add_users">
										<h4>Имя</h4>
										<input type="text" class="add_users" value="" placeholder="<?php echo $firstname; ?>" name="firstname" required disabled>
									</div>
								</td>
								<td>
									<div class="form-row add_users">
										<h4>Отчество*</h4>
										<input type="text" class="add_users" value="" placeholder="<?php echo $secondname; ?>" name="secondname" disabled>
									</div>
								</td>
							</tr>
							<tr>
								<td>
									<div class="form-row add_users">
										<h4>Дата рождения</h4>
										<input type="text" class="add_users" value="" placeholder="<?php echo $date_birth; ?>" name="date_birth" required disabled>
									</div>
								</td>
								<td>
									<div class="form-row select-arrow course_finder">
										<h4>Пол</h4>
										<select name="select_gender" class="select_teacher" required style="background-color:#f2f2f2;">
											<option value="0">Женщина</option>
											<option value="1">Мужчина</option>
										</select>
									</div>
								</td>
							</tr>
							<tr>
								<td>
									<div class="form-row-select select-arrow  course_finder" action="#" method="post">
										<h4>Должность</h4>
										<select name="select_post" class="select_teacher" required style="background-color:#f2f2f2;">
											<option value="0">Аспирант</option>
											<option value="1">Ассистент</option>
											<option value="2">Ведущий научный сотрудник</option>
											<option value="3">Главный научный сотрудник</option>
											<option value="4">Докторант</option>
											<option value="5">Доцент</option>
											<option value="6">Младший научный сотрудник</option>
											<option value="7">Научный сотрудник</option>
											<option value="8">Преподаватель</option>
											<option value="9">Профессор</option>
											<option value="10">Стажер</option>
											<option value="11">Старший научный сотрудник</option>
											<option value="12">Старший преподаватель</option>
											<option value="13">Студент</option>
										</select>
									</div>
								</td>
								<td>
									<div class="form-row-select select-arrow course_finder">
										<h4>Ученое звание</h4>
										<select name="select_rank" class="select_teacher" style="background-color:#f2f2f2;">
											<option value="0"></option>
											<option value="1">Действительный член (академик) Академии наук</option>
											<option value="2">Доцент</option>
											<option value="3">Профессор</option>
											<option value="4">Член-корресподент (член-корр.) Академии наук</option>
										</select>
									</div>
								</td>
								<td>
									<div class="form-row-select select-arrow course_finder">
										<h4>Ученая степень</h4>
										<select name="select_degree" class="select_teacher" style="background-color:#f2f2f2;">
											<option value="0"></option>
											<option value="1">Доктор наук</option>
											<option value="2">Кандидат наук</option>
										</select>
									</div>
								</td>
							</tr>
							<tr>
								<td>
									<div class="form-row add_users">
										<h4>Контактный телефон</h4>
										<input type="text" class="add_users" placeholder="<?php echo $telephone;?>" name="telephone" id="telephone" required disabled>
									</div>
								</td>
								<td>
									<div class="form-row add_users">
										<h4>E-mail</h4>
										<input type="text" class="add_users" placeholder="<?php echo $email;?>" name="email" id="e-mail" required disabled>
									</div>
								</td>
								<td>
									<div class="form-row add_users" id="account_password_user">
										<h4>Пароль</h4>
										<input type="password" class="add_users" name="account_password" id="account_password" value="" required disabled/>
									</div>
								</td>
							</tr>

						</table>
						<div class="button_tabs">
							<button type="submit" name="save_user" class="cws-button bt-color-4 border-radius alt" disabled>Сохранить</button>
							<button type="button" name="edit_user" class="cws-button bt-color-4 border-radius alt" 
							onclick = "document.getElementsByName('lastname').removeAttr('disabled');">Редактировать</button>
						</div>
					</div>
					</form>
				</div>
				<?php
						/*Проверка нажатия на кнопку сохронить*/
						if (isset($_POST['save_user'])) {
							/*Проверка разрешения на обработку данных*/
							if ($_POST['obrabot'] != 0) {
								reg();
							}
							else{
								echo "Нужно согласие на оброботку данных";
							}
						}
				?>
				<div class="container-tabs active" data-tabs-id="cont-tabs2">
					<div class="widget-contact-form">
						<form method="post" class="contact-form  alt clear-fix">
							<div class="text_long add_publ">
								<h4>Название научного труда</h4>
								<textarea class="text_long" name="name_public" cols="20" placeholder="Введите полное наименование публикации" aria-invalid="false" required></textarea>
							</div>
							<table class = "avtor">
								<tr>
									<td>
										<div class="form-row add_publ">
											<h4>Автор</h4>
											<div class="form-row-select-1 select-arrow course_finder">
												<select name="avtor_name" class="select_teacher">
												    <option></option>
													<?php
														require('includes/avtor.php');
													?>
												</select>
											</div>
										</div>
									</td>
									<td style="position:  absolute; margin-top: 30px;">
										<div class="cws-button add-author border-radius" id = "new_avtor"><a href="javascript:PopUpShow1()">Добавить</a></div>
										<div class="cws-button add-author bt-color-5 border-radius" id = "new_avtor"><a href="javascript:PopUpShow()">Автора нет в списке</a></div>
									</td>

									</td>
								</tr>
								<tr class="b-popup" id="popup1">
								<td>
								<div>
                                    <div class="b-popup-content">
                                        <div class="form-row add_publ">
									        <h4>ФИО автора</h4>
									        <input type="text" class="add_publ" name="new_avtor_1">
								        </div>
                                    </div>
                                </div>
                                </td>
								</tr>
								<tr class="b-popup" id="popup2">
								<td>
								<div>
                                    <div class="form-row add_publ">
											<h4>Автор</h4>
											<div class="form-row-select-1 select-arrow course_finder">
												<select name="avtor_name_1" class="select_teacher">
												<option></option>
													<?php
														require('includes/avtor.php');
													?>
												</select>
											</div>
										</div>
                                </div>
                                </td>
                                <td>
                                     <div class="cws-button add-author border-radius" id = "new_avtor1"><a href="javascript:PopUpShow2()">Добавить</a></div>
                                </td>
								</tr>
								<tr class="b-popup" id="popup3">
								<td>
								<div>
                                    <div class="form-row add_publ">
											<h4>Автор</h4>
											<div class="form-row-select-1 select-arrow course_finder">
												<select name="avtor_name_2" class="select_teacher">
												<option></option>
													<?php
														require('includes/avtor.php');
													?>
												</select>
											</div>
										</div>
                                </div>
                                </td>
								</tr>
							</table>

							<div class="select_type">
								<div class="form-row add_publ">
									<h4>Тип публикации</h4>
									<select name="select_type" class="select_teacher" required>
										<option value="0" placeholder="Должность"></option>
										<option value="Дипломы, награды, премии">Дипломы, награды, премии</option>
										<option value="Диссертация">Диссертация</option>
										<option value="Монография">Монография</option>
										<option value="Научно-исследовательская работа">Научно-исследовательская работа</option>
										<option value="Научная статья">Научная статья</option>
										<option value="Учебное пособие">Учебное пособие</option>
										<option value="Патент">Патент</option>
									</select>
								</div>

								<div class="form-row add_publ">
									<h4>Объем</h4>
									<input type="text" class="add_publ" value="" placeholder="" name="num" required>
								</div>
								<div class="form-row add_publ">
									<h4>Год публикации</h4>
									<input type="text" class="add_publ" name="date" required>
								</div>
							</div>
							<div class="text_long add_publ">
								<h4>Издательство</h4>
								<textarea class="text_long" name="izdat" cols="20" placeholder="Введите полное наименование издательства" aria-invalid="false" required ></textarea>
							</div>
							<div class="button_tabs">
								<button type="submit" name="add_doc" value="1" class="cws-button border-radius alt">Добавить</button>
							</div>
						</form>
					</div>
				</div>

				<div class="container-tabs" data-tabs-id="cont-tabs3">
					<section>
						<div align="center" class="clear-fix">
							<?php echo publicationRender(publicationData()); ?>
						<div class="button_tabs">
							<a href="download.php" target="_blank" name="save_user" value="1" class="cws-button bt-color-4 border-radius alt">Печать отчета</a>

					</section>
				</div>

				<div class="container-tabs" data-tabs-id="cont-tabs4">
<div class="container_search">
		<div class="current color-2 frm_search">
			<form name="frm_search" method="post">
				<i class="fa fa-search fa-3x icon"></i>
				<div class="input_search">
					<br><h3>Наименование публикации</h3>
					<input type="text" placeholder="Что ищем ..." name="name_public">
				</div>
				<div class="input_search">
					<h3>Автор публикации</h3>
					<input type="text" placeholder="Кого ищем ..." name="name_author">
				</div>
				<div class="input_search">
					<h3>Тип публикации</h3>
					<div>
						<div class="checkbox_search">
							<input type="checkbox" id="checkbox1" name="type_article_1" value= "Научная статья" />
							<label for="checkbox1">&nbsp; - научная статья</label>
						</div>
						<div class="checkbox_search">
							<input type="checkbox" id="checkbox2" name="type_article_2" value= "Монография" />
							<label for="checkbox2">&nbsp; - монография</label>
						</div>
						<div class="checkbox_search">
							<input type="checkbox" id="checkbox3" name="type_article_3" value= "диссертации" />
							<label for="checkbox3">&nbsp; - диссертации</label>
						</div>
						<div class="checkbox_search">
							<input type="checkbox" id="checkbox4" name="type_article_4" value = "дипломы, награды, премии" />
							<label for="checkbox4">&nbsp; - дипломы, награды, премии</label>
						</div>
						<div class="checkbox_search">
							<input type="checkbox" id="checkbox5" name="type_article_5" value = "учебное пособие" />
							<label for="checkbox5">&nbsp; - учебное пособие</label>
						</div>

						<div class="checkbox_search">
							<input type="checkbox" id="checkbox6" name="type_article_6" value = "научно-исследовательская работа" />
							<label for="checkbox6">&nbsp; - научно-исследовательская работа</label>
						</div>
						<div class="checkbox_search">
							<input type="checkbox" id="checkbox7" name="type_article_7" value = "патент" />
							<label for="checkbox7">&nbsp; - патент</label>
						</div>

					</div>
				</div>
				<div class="clear"> </div>
				<table>
					<tr>
						<td><h3>Год публикации</h3></td>
						<td>
							<div class="input_search course_year select-arrow">
								<select id="select_year" class="select_year" name='year_public_to' >
									<option value=""></option>
									<option value="2000">2000</option>
									<option value="2001">2001</option>
									<option value="2002">2002</option>
									<option value="2003">2003</option>
									<option value="2004">2004</option>
									<option value="2005">2005</option>
									<option value="2006">2006</option>
									<option value="2007">2007</option>
									<option value="2008">2008</option>
									<option value="2009">2009</option>
									<option value="2010">2010</option>
									<option value="2011">2011</option>
									<option value="2012">2012</option>
									<option value="2013">2013</option>
									<option value="2014">2014</option>
									<option value="2015">2015</option>
									<option value="2016">2016</option>
									<option value="2017">2017</option>
									<option value="2018">2018</option>
								</select>
								-
								<select class="select_year" name='year_public_from'>
									<option value=""></option>
									<option value="">2018</option>
									<option value="">2017</option>
									<option value="">2016</option>
									<option value="">2015</option>
									<option value="">2014</option>
									<option value="">2013</option>
									<option value="">2012</option>
									<option value="">2011</option>
									<option value="">2010</option>
									<option value="">2009</option>
									<option value="">2008</option>
									<option value="">2007</option>
									<option value="">2006</option>
									<option value="">2005</option>
									<option value="">2004</option>
									<option value="">2003</option>
									<option value="">2002</option>
									<option value="">2001</option>
									<option value="">2000</option>
								</select>
							</div>
						</td>
					</tr>
				</table>
				<br><button type="reset" class="small button_reset">Очистить</button>
				<button type="submite" class="cws-button bt-color-3 border-radius button_search" name = "btn_search">Найти &nbsp;<i class="fa fa-angle-right"></i></button>
			</form>
		</div>
	</div>
	<?php
		//Подключаем файл который осуществляет поиск
		if (isset($_POST['btn_search'])) {
			require('includes/search.php');
	?>
	<section>
		<div align="center" class="clear-fix">
			<div class="grid-col-6">
				<div class="banner-offer icon-right bg-color-1">
					<div class="banner-text">Результаты поискового запроса</div>
					<div class="banner-icon">
						<i class="fa fa-graduation-cap"></i>
					</div>
				</div>
			</div>
			<table class="result_search">
				<tr>
					<th>№</th>
					<th>Название научного труда</th>
					<th>Год публикации</th>
					<th>Тип публикации</th>
					<th>Количество страниц</th>
					<th>Автор</th>
				</tr>
				<?php
					
					while ($result = mysql_fetch_array($sql_search)) {
						$i++;
						echo "<tr>
								<td>".$i."</td>
			 					<td>".$result['name_public']."</td>
			  					<td>".$result['year_public']."</td>
			 					<td>".$result['type_public']."</td>
			 					<td>".$result['num_public']."</td>
			 					<td>".$result['name_author']."</td>
							</tr>";
					}
					
				?>
			</table>
		</div>
	</section>
	<?php
		}
	?>
				</div>
			</div>
	</div>





	<footer>
		<div class="footer-bottom">
			<div class="grid-row clear-fix">
				<div class="copyright">QuickR<span></span> 2017 г. Разработчик: Декун Надежда, НМТ-443907.</div>
				<nav class="footer-nav">
					<ul class="clear-fix">
							<li>
								<a href="index.php">Поиск</a>
							</li>
							<li>
								<a href="news.php" >Новости</a>
							</li>
              <?php
                    if(isset($_SESSION["session_username"])){
              ?>
              <li>
                <a href="authorization.php">Личный кабинет</a>
              </li>
              <?php
              }else{
              ?>
							<li>
								<a>Войти</a>
							</li>
							<li>
								<a href="login.php">Зарегистрироваться</a>
							</li>
              <?php
                }
              ?>
						</ul>
				</nav>
			</div>
		</div>
	</footer>

	
	
	
<script src="http://code.jquery.com/jquery-2.0.2.min.js"></script>
<script>
    $(document).ready(function(){
        //Скрыть PopUp при загрузке страницы    
        PopUpHide();
    });
    //Функция отображения PopUp
    function PopUpShow(){
        $("#popup1").show();
    }
    function PopUpShow1(){
        $("#popup2").show();
        $("#new_avtor").hide();
    }
     function PopUpShow2(){
        $("#popup3").show();
        $("#new_avtor1").hide();
    }
    //Функция скрытия PopUp
    function PopUpHide(){
        $("#popup1").hide();
        $("#popup2").hide();
        $("#popup3").hide();
    }
</script>
	<script src="js/jquery.min.js"></script>

	<script src="js/main.js"></script>
</body>
</html>
# QuickR
### Что я сделал:
* Установил [DomPDF](https://github.com/dompdf/dompdf)
* Добавил файл `./download.php` для генерации PDF Из HTML
* А HTML файл генерируется в `includes/publication.php`. Туда я добавил 3 функции `publicationData()`, `publicationRender($data)`, `renderPdf($data)`. По названию Все должно быть понятно.
* Удалил функцию `publications()` из `includes/publication.php`, если где нибудь появлится ошибка типа `Call to undefined function ...`, то надо новые функции вызвать вместо старой
> Старая версия, которая была до меня находится в папке `_addons\old_version`, там же новый дамп базы данных, старый дамп и прочие данные.
### Ошибка в консоли, проверь потом
* в `authorization.php`:
Failed to load resource: the server responded with a status of 404 (Not Found)
     main.js:1644 Uncaught TypeError: $(...).validate is not a function
         at HTMLFormElement.<anonymous> (main.js:1644)
         at Function.each (jquery.min.js:2)
         at m.fn.init.each (jquery.min.js:2)
         at main.js:1642
     font-awesome:1 Failed to load resource: the server responded with a status of 404 (Not Found)
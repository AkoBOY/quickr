-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Июн 13 2018 г., 01:47
-- Версия сервера: 5.7.19
-- Версия PHP: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `h98263l4_admin`
--

-- --------------------------------------------------------

--
-- Структура таблицы `t_sprav_academic_title`
--

CREATE TABLE `t_sprav_academic_title` (
  `academ_title _id` int(10) NOT NULL,
  `name` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `t_sprav_academic_title`
--

INSERT INTO `t_sprav_academic_title` (`academ_title _id`, `name`) VALUES
(0, 'Нет'),
(1, 'Доцент'),
(2, 'Профессор');

-- --------------------------------------------------------

--
-- Структура таблицы `t_sprav_conference`
--

CREATE TABLE `t_sprav_conference` (
  `conf_id` int(10) NOT NULL,
  `name_conf` varchar(255) NOT NULL,
  `city_conf` varchar(64) NOT NULL,
  `place_conf` varchar(128) NOT NULL,
  `date_conf` date NOT NULL,
  `publish_conf` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `t_sprav_conference`
--

INSERT INTO `t_sprav_conference` (`conf_id`, `name_conf`, `city_conf`, `place_conf`, `date_conf`, `publish_conf`) VALUES
(0, 'фыавфыва', 'фывафыва', 'фывафыва', '2018-05-01', 'фывафыва'),
(1, 'Умная', 'Москва', 'школа', '2018-05-16', 'фыв'),
(2, 'фыавфыва', 'фывафыва', 'фывафыва', '2018-05-01', 'фывафыва');

-- --------------------------------------------------------

--
-- Структура таблицы `t_sprav_degree`
--

CREATE TABLE `t_sprav_degree` (
  `degree_id` int(10) NOT NULL,
  `name` varchar(64) NOT NULL,
  `fullname` varchar(255) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `t_sprav_degree`
--

INSERT INTO `t_sprav_degree` (`degree_id`, `name`, `fullname`) VALUES
(1, 'к.т.н.', 'кандидат технических наук'),
(2, 'д.т.н.', 'доктор технических наук'),
(4, 'Нет', 'Нет');

-- --------------------------------------------------------

--
-- Структура таблицы `t_sprav_institution`
--

CREATE TABLE `t_sprav_institution` (
  `inst_id` int(10) NOT NULL,
  `name` varchar(255) NOT NULL,
  `fullname` varchar(255) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `t_sprav_institution`
--

INSERT INTO `t_sprav_institution` (`inst_id`, `name`, `fullname`) VALUES
(1, 'УрФУ', 'УрФУ имени первого Президента России Б.Н. Ельцина');

-- --------------------------------------------------------

--
-- Структура таблицы `t_sprav_ journal`
--

CREATE TABLE `t_sprav_ journal` (
  `journal_id` int(10) NOT NULL,
  `type_id` int(10) NOT NULL,
  `name_ journal` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `t_sprav_ journal`
--

INSERT INTO `t_sprav_ journal` (`journal_id`, `type_id`, `name_ journal`) VALUES
(1, 1, 'бла бла бла');

-- --------------------------------------------------------

--
-- Структура таблицы `t_sprav_posts`
--

CREATE TABLE `t_sprav_posts` (
  `post_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `t_sprav_posts`
--

INSERT INTO `t_sprav_posts` (`post_id`, `name`) VALUES
(1, 'Аспирант'),
(2, 'Ассистент'),
(3, 'Ведущий научный сотрудник'),
(4, 'Главный научный сотрудник'),
(5, 'Докторант'),
(6, 'Доцент'),
(7, 'Младший научный сотрудник'),
(8, 'Научный сотрудник'),
(9, 'Преподаватель'),
(10, 'Профессор'),
(11, 'Старший научный сотрудник'),
(12, 'Старший преподаватель'),
(13, 'Студент');

-- --------------------------------------------------------

--
-- Структура таблицы `t_sprav_publication`
--

CREATE TABLE `t_sprav_publication` (
  `public_id` int(10) NOT NULL,
  `name_public` varchar(128) NOT NULL,
  `type_public` varchar(64) NOT NULL,
  `year_public` year(4) NOT NULL,
  `name_author` varchar(100) NOT NULL DEFAULT 'Не указан',
  `name_avtor_1` varchar(1000) DEFAULT NULL,
  `name_avtor_2` varchar(1000) DEFAULT NULL,
  `name_avtor_3` varchar(1000) DEFAULT NULL,
  `conf_id` int(10) DEFAULT NULL,
  `num_public` int(10) NOT NULL,
  `date_registr` date NOT NULL,
  `journal_id` int(10) DEFAULT NULL,
  `num_tome` int(4) DEFAULT NULL,
  `num_journal` int(10) DEFAULT NULL,
  `num_DOI` varchar(128) DEFAULT NULL,
  `izdat` varchar(200) NOT NULL,
  `user` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `t_sprav_publication`
--

INSERT INTO `t_sprav_publication` (`public_id`, `name_public`, `type_public`, `year_public`, `name_author`, `name_avtor_1`, `name_avtor_2`, `name_avtor_3`, `conf_id`, `num_public`, `date_registr`, `journal_id`, `num_tome`, `num_journal`, `num_DOI`, `izdat`, `user`) VALUES
(2, 'ОСОБЕННОСТИ ПЕРЕХОДНЫХ ПРОЦЕССОВ ДОМЕННОЙ ПЛАВКИ', 'Научная статья', 2017, 'Петров', '', '', '', NULL, 5, '2018-06-11', NULL, NULL, NULL, NULL, 'Металлург', ''),
(3, 'Основные направления ресурсо и энергосбережения в доменном производстве', 'Научная статья', 2016, 'Петров', '', '', '', NULL, 6, '2018-06-11', NULL, NULL, NULL, NULL, 'Черная металлургия', ''),
(12, 'СОВРЕМЕННАЯ МЕТОДОЛОГИЯ И КОМПЬЮТЕРНЫЕ ТЕХНОЛОГИИ СОЗДАНИЯ ПРОГРАММНОГО ОБЕСПЕЧЕНИЯ МОДЕЛЬНЫХ СИСТЕМ ПОДДЕРЖКИ ПРИНЯТИЯ РЕШЕНИЙ ', 'Научная статья', 2017, '', '', '', '', NULL, 8, '2018-06-12', NULL, NULL, NULL, NULL, 'Izvestiya Vysshikh Uchebnykh Zavedenij. Chernaya Metallurgiya. ', ''),
(13, 'Тест', 'Монография', 2018, 'Мангилев', '', '', '', NULL, 12, '2018-06-12', NULL, NULL, NULL, NULL, 'ТЕСТ ТЕСТ', 'leomang1488@gmail.com'),
(14, 'Разработка программного обеспечения для диагностики вида отклонения доменной плавки от нормального режима', 'Научная статья', 2016, 'Лавров Владислав Васильевич', '', '', '', NULL, 6, '2018-06-12', NULL, NULL, NULL, NULL, 'Вестник Томского государственного университета. Управление, вычислительная техника и информатика.', ''),
(15, 'Разработка программного обеспечения для диагностики вида отклонения доменной плавки от нормального режима', 'Научная статья', 2016, 'Лавров Владислав Васильевич', '', '', '', NULL, 6, '2018-06-12', NULL, NULL, NULL, NULL, 'Вестник Томского государственного университета. Управление, вычислительная техника и информатика.', '');

-- --------------------------------------------------------

--
-- Структура таблицы `t_sprav_publication_author`
--

CREATE TABLE `t_sprav_publication_author` (
  `author_id` int(10) NOT NULL,
  `name_author` varchar(64) NOT NULL,
  `date_registration_author` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `t_sprav_publication_author`
--

INSERT INTO `t_sprav_publication_author` (`author_id`, `name_author`, `date_registration_author`) VALUES
(1, 'Иванов', '2018-06-02'),
(6, 'Лавров Владислав Васильевич', '2018-06-12'),
(7, 'Мангилев', '2018-06-12'),
(9, 'Истомин, А. С.', '2018-06-12');

-- --------------------------------------------------------

--
-- Структура таблицы `t_type_publication`
--

CREATE TABLE `t_type_publication` (
  `type_id` int(10) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `t_type_publication`
--

INSERT INTO `t_type_publication` (`type_id`, `name`) VALUES
(1, 'монография');

-- --------------------------------------------------------

--
-- Структура таблицы `t_users`
--

CREATE TABLE `t_users` (
  `user_id` int(10) NOT NULL,
  `lastname` varchar(64) NOT NULL,
  `firstname` varchar(64) NOT NULL,
  `secondname` varchar(64) NOT NULL,
  `date` varchar(1000) NOT NULL,
  `sex` int(11) NOT NULL,
  `inst_id` int(10) NOT NULL,
  `post_id` int(10) NOT NULL,
  `academ_title_id` int(10) NOT NULL,
  `degree_id` int(10) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `email` varchar(32) NOT NULL,
  `password` varchar(32) NOT NULL,
  `reg_ip` varchar(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `t_users`
--

INSERT INTO `t_users` (`user_id`, `lastname`, `firstname`, `secondname`, `date`, `sex`, `inst_id`, `post_id`, `academ_title_id`, `degree_id`, `phone`, `email`, `password`, `reg_ip`) VALUES
(1, 'Гурин', 'Иван', 'Александрович', '0000-00-00', 0, 1, 0, 0, 0, '', 'sportsoft@mail.ru', '12345', '1474340073'),
(6, 'Мангилев', 'Леонид', 'Сергеевич', '08-10-1998', 1, 0, 0, 3, 2, '+7 (121) 241-2412', 'leomang1488@gmail.com', '440012', '127.0.0.1'),
(9, 'Лавров', 'Владислав', 'Васильевич', '10.12.1970', 1, 0, 8, 3, 1, '+79221634727', 'lavrov.vladislav@gmail.com', 'lavrov12345', '212.193.94.23');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `t_sprav_academic_title`
--
ALTER TABLE `t_sprav_academic_title`
  ADD PRIMARY KEY (`academ_title _id`);

--
-- Индексы таблицы `t_sprav_conference`
--
ALTER TABLE `t_sprav_conference`
  ADD PRIMARY KEY (`conf_id`);

--
-- Индексы таблицы `t_sprav_degree`
--
ALTER TABLE `t_sprav_degree`
  ADD PRIMARY KEY (`degree_id`);

--
-- Индексы таблицы `t_sprav_institution`
--
ALTER TABLE `t_sprav_institution`
  ADD PRIMARY KEY (`inst_id`);

--
-- Индексы таблицы `t_sprav_ journal`
--
ALTER TABLE `t_sprav_ journal`
  ADD UNIQUE KEY `K` (`journal_id`),
  ADD UNIQUE KEY `K1` (`type_id`);

--
-- Индексы таблицы `t_sprav_posts`
--
ALTER TABLE `t_sprav_posts`
  ADD PRIMARY KEY (`post_id`);

--
-- Индексы таблицы `t_sprav_publication`
--
ALTER TABLE `t_sprav_publication`
  ADD PRIMARY KEY (`public_id`),
  ADD UNIQUE KEY `P` (`conf_id`),
  ADD KEY `journal_id` (`journal_id`);

--
-- Индексы таблицы `t_sprav_publication_author`
--
ALTER TABLE `t_sprav_publication_author`
  ADD UNIQUE KEY `K` (`author_id`);

--
-- Индексы таблицы `t_type_publication`
--
ALTER TABLE `t_type_publication`
  ADD PRIMARY KEY (`type_id`);

--
-- Индексы таблицы `t_users`
--
ALTER TABLE `t_users`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `inst_id` (`inst_id`,`post_id`),
  ADD UNIQUE KEY `academ_title_id` (`academ_title_id`,`degree_id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `degree_id` (`degree_id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `t_sprav_degree`
--
ALTER TABLE `t_sprav_degree`
  MODIFY `degree_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `t_sprav_institution`
--
ALTER TABLE `t_sprav_institution`
  MODIFY `inst_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `t_sprav_ journal`
--
ALTER TABLE `t_sprav_ journal`
  MODIFY `journal_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `t_sprav_posts`
--
ALTER TABLE `t_sprav_posts`
  MODIFY `post_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT для таблицы `t_sprav_publication`
--
ALTER TABLE `t_sprav_publication`
  MODIFY `public_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT для таблицы `t_sprav_publication_author`
--
ALTER TABLE `t_sprav_publication_author`
  MODIFY `author_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT для таблицы `t_type_publication`
--
ALTER TABLE `t_type_publication`
  MODIFY `type_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `t_users`
--
ALTER TABLE `t_users`
  MODIFY `user_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

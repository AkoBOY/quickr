<?php
session_start();
require_once("includes/connect.php");
require_once("includes/register.php");
require_once("includes/stat.php");
require_once("includes/news.php");
    /*Проверка полей авторизации*/
    if(isset($_POST["login"])){
      if(!empty($_POST['username']) && !empty($_POST['password'])) {
      $username=htmlspecialchars($_POST['username']);
      $password=htmlspecialchars($_POST['password']);
      $query_avto = mysql_query("SELECT * FROM `t_users` WHERE `email` ='$username' AND `password` ='$password'");
      $numrows=mysql_num_rows($query_avto);
    if($numrows!=0)
        {
        	/*Добавление в кеш*/
          $_SESSION['session_username'] =$username; 

          /* Перенаправление браузера */
          echo "<META HTTP-EQUIV='REFRESH' CONTENT='0;URL= authorization.php'> ";
  } 
      else {
              $message = "Неправильный пароль или логин";
            }
  } 
}
  ?>
<html>
<head>
    <title>QuickR-Отчет по НИИ</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
    <!-- style -->
    <link rel="shortcut icon" href="img/favicon.png">
    <link rel="stylesheet" href="fi/flaticon.css">
    <link rel="stylesheet" href="css/font-awesome.css">
    <link rel="stylesheet" href="css/main.css">


</head>
<body class="pc">
	    <header class="only-color">
		<div class="sticky-wrapper">
			<div class="sticky-menu">
				<div class="grid-row clear-fix">
					<a href="index.php" class="logo">
						<img src="img/logo.png">
						<h1>QuickR</h1>
					</a>
					<nav class="main-nav">

						<i class="mobile_menu_switcher"></i>
						<ul class="clear-fix">
							<li id="menu-item-0">
								<a href="index.php" class="active">Поиск</a>
							</li>
							<li id="menu-item-3">
								<a href="news.php" >Новости</a>
							</li>
              <?php
                    if(isset($_SESSION["session_username"])){
              ?>
              <li id="menu-item-2">
								<a href="authorization.php" class="active">Личный кабинет</a>
								<div class="login-block">
									<form class="login-form">
										<a href="logout.php">Выйти</a>
									</form>
								</div>
							</li>
              <?php
              }else{
              ?>
							<li id="menu-item-2">
								<a>Войти</a>
								<div class="login-block">

									<h2>Авторизация</h2>
									<form class="login-form" method="post">
										<div class="form-group">
											<input type="text" class="login-input" placeholder="E-mail" name="username">
											<span class="input-icon">
												<i class="fa fa-user"></i>
											</span>
										</div>
										<div class="form-group">
											<input type="password" class="login-input" name = 'password' onblur="if(this.value=='')this.value='Пароль'" onfocus="if(this.value=='Пароль')this.value='' " placeholder="Пароль">
											<span class="input-icon">
												<i class="fa fa-lock"></i>
											</span>
										</div>
										<p class="small">
											<a href="">Забыли пароль?</a>
										</p>
										<button name = 'login' class="btn_log cws-button bt-color-5 border-radius">Войти</button>
										<?php
											echo $message;
										?>
									</form>
								</div>
							</li>
							<li id="menu-item-3">
								<a href="login.php">Зарегистрироваться</a>
							</li>
              <?php
                }
              ?>
						</ul>
					</nav>
				</div>
			</div>
		</div>
    </header>
	<div class="intro container_search">
		<div class="current color-2 frm_search">
			<form name="frm_search" method="post">
				<i class="fa fa-search fa-3x icon"></i>
				<div class="input_search">
					<br><h3>Наименование публикации</h3>
					<input type="text" placeholder="Что ищем ..." name="name_public">
				</div>
				<div class="input_search">
					<h3>Автор публикации</h3>
					<input type="text" placeholder="Кого ищем ..." name="name_author">
				</div>
				<div class="input_search">
					<h3>Тип публикации</h3>
					<div>
						<div class="checkbox_search">
							<input type="checkbox" id="checkbox8" name="type_article_1" value = "0">
							<input type="checkbox" id="checkbox1" name="type_article_1" value= "Научная статья" />
							<label for="checkbox1">&nbsp; - научная статья</label>
						</div>
						<div class="checkbox_search">
							<input type="checkbox" id="checkbox9" name="type_article_2" value = "0">
							<input type="checkbox" id="checkbox2" name="type_article_2" value= "Монография" />
							<label for="checkbox2">&nbsp; - монография</label>
						</div>
						<div class="checkbox_search">
							<input type="checkbox" id="checkbox10" name="type_article_3" value = "0">
							<input type="checkbox" id="checkbox3" name="type_article_3" value= "диссертации" />
							<label for="checkbox3">&nbsp; - диссертации</label>
						</div>
						<div class="checkbox_search">
							<input type="checkbox" id="checkbox11" name="type_article_4" value = "0">
							<input type="checkbox" id="checkbox4" name="type_article_4" value = "дипломы, награды, премии" />
							<label for="checkbox4">&nbsp; - дипломы, награды, премии</label>
						</div>
						<div class="checkbox_search">
							<input type="checkbox" id="checkbox12" name="type_article_5" value = "0">
							<input type="checkbox" id="checkbox5" name="type_article_5" value = "учебное пособие" />
							<label for="checkbox5">&nbsp; - учебное пособие</label>
						</div>

						<div class="checkbox_search">
							<input type="checkbox" id="checkbox13" name="type_article_6" value = "0">
							<input type="checkbox" id="checkbox6" name="type_article_6" value = "научно-исследовательская работа" />
							<label for="checkbox6">&nbsp; - научно-исследовательская работа</label>
						</div>
						<div class="checkbox_search">
							<input type="checkbox" id="checkbox14" name="type_article_7" value = "0">
							<input type="checkbox" id="checkbox7" name="type_article_7" value = "патент" />
							<label for="checkbox7">&nbsp; - патент</label>
						</div>

					</div>
				</div>
				<div class="clear"> </div>
				<table>
					<tr>
						<td><h3>Год публикации</h3></td>
						<td>
							<div class="input_search course_year select-arrow">
								<select id="select_year" class="select_year" name='year_public_to' >
									<option value=""></option>
									<option value="2000">2000</option>
									<option value="2001">2001</option>
									<option value="2002">2002</option>
									<option value="2003">2003</option>
									<option value="2004">2004</option>
									<option value="2005">2005</option>
									<option value="2006">2006</option>
									<option value="2007">2007</option>
									<option value="2008">2008</option>
									<option value="2009">2009</option>
									<option value="2010">2010</option>
									<option value="2011">2011</option>
									<option value="2012">2012</option>
									<option value="2013">2013</option>
									<option value="2014">2014</option>
									<option value="2015">2015</option>
									<option value="2016">2016</option>
									<option value="2017">2017</option>
									<option value="2018">2018</option>
								</select>
								-
								<select class="select_year" name='year_public_from'>
									<option value=""></option>
									<option value="">2018</option>
									<option value="">2017</option>
									<option value="">2016</option>
									<option value="">2015</option>
									<option value="">2014</option>
									<option value="">2013</option>
									<option value="">2012</option>
									<option value="">2011</option>
									<option value="">2010</option>
									<option value="">2009</option>
									<option value="">2008</option>
									<option value="">2007</option>
									<option value="">2006</option>
									<option value="">2005</option>
									<option value="">2004</option>
									<option value="">2003</option>
									<option value="">2002</option>
									<option value="">2001</option>
									<option value="">2000</option>
								</select>
							</div>
						</td>
					</tr>
				</table>
				<br><button type="reset" class="small button_reset">Очистить</button>
				<button type="submite" class="cws-button bt-color-3 border-radius button_search" name = "btn_search">Найти &nbsp;<i class="fa fa-angle-right"></i></button>
			</form>
		</div>
	</div>
	<?php
		//Подключаем файл который осуществляет поиск
		if (isset($_POST['btn_search'])) {
			require('includes/search.php');
	?>
	<hr class="divider-color">
	<section>
		<div align="center" class="clear-fix">
			<div class="grid-col-6">
				<div class="banner-offer icon-right bg-color-1">
					<div class="banner-text">Результаты поискового запроса</div>
					<div class="banner-icon">
						<i class="fa fa-graduation-cap"></i>
					</div>
				</div>
			</div>
			<table class="result_search">
				<tr>
					<th>№</th>
					<th>Название научного труда</th>
					<th>Год публикации</th>
					<th>Тип публикации</th>
					<th>Количество страниц</th>
					<th>Автор</th>
				</tr>
				<?php
					
					while ($result = mysql_fetch_array($sql_search)) {
						$i++;
						echo "<tr>
								<td>".$i."</td>
			 					<td>".$result['name_public']."</td>
			  					<td>".$result['year_public']."</td>
			 					<td>".$result['type_public']."</td>
			 					<td>".$result['num_public']."</td>
			 					<td>".$result['name_author']."</td>
							</tr>";
					}
					
				?>
			</table>
		</div>
		<div class="page_pagination clear-fix">
			<a href="#"><i class="fa fa-angle-double-left"></i></a>
			<!--
			-->
			<a href="#" class="active">1</a>
			<!--
			-->
			<a href="#">2</a>
			<!--
			-->
			<a href="#">3</a>
			<!--
			-->
			<a href="#"><i class="fa fa-angle-double-right"></i></a>
		</div>
	</section>
	<?php
		}
	?>
	<hr class="divider-color">
	<section class="padding-section">
		<div class="grid-row clear-fix">
			<div class="grid-col-row">
				<div class="grid-col grid-col-4">
					<div class="current color-1">
						<h4>Создай свой отчет</h4>
						<div class="current-logo">
							<i class="flaticon-award1"></i>
						</div>
						<div class="info-block">
							<p><i class="fa fa-search fa-2x" aria-hidden="true"></i>&nbsp; Находи научные работы  </p>
							<p><i class="fa fa-user fa-2x" aria-hidden="true"></i>&nbsp; Регистрируйся на сайте </p>
							<p><i class="fa fa-book fa-2x" aria-hidden="true"></i>&nbsp; Публикуй свои работы   </p>
							<p><i class="fa fa-globe fa-2x" aria-hidden="true"></i>&nbsp; Будь в курсе новостей </p>
							<a href="login.php" class="cws-button bt-color-1 border-radius button_report">Начать</a>
						</div>
					</div>
				</div>
				<div class="grid-col grid-col-4">
					<div class="current">
						<h5>Новости</h5>
						<div class="current-logo">
							<i class="flaticon-calendar"></i>
						</div>
						<div class="info-block">
							<?php
								news_index()
							?>
							<a href="news.php" class="cws-button bt-color-2 border-radius button_news">Другие новости</a>
						</div>
					</div>
				</div>
				<div class="grid-col grid-col-4">
					<div class="current color-2">
						<h6>Публикации в числах</h6>
						<div class="current-logo">
							<i class="flaticon-pie"></i>
						</div>
						<div class="info-block">
							<table class="table_news">
								<tr>
									<td>Общее число научных статей:</td>
									<td><? echo $statia_num ?></td>
								</tr>
								<tr>
									<td>Общее число учебный пособий:</td>
									<td><? echo $posb_num ?></td>
								</tr>
								<tr>
									<td>Общее число монографий:</td>
									<td><? echo $mono_num ?></td>
								</tr>
								<tr>
									<td>Общее число патентов:</td>
									<td><? echo $patent_num ?></td>
								</tr>
								<tr>
									<td>Общее число диссертаций:</td>
									<td><? echo $diser_num ?></td>
								</tr>
								<tr>
									<td>Общее число публикаций:</td>
									<td><? echo $public_num ?></td>
								</tr>
								<tr>
									<td>Число зарегистрированных пользователей:</td>
									<td><? echo $user_num ?></td>
								</tr>
								<tr>
									<td>Общее число авторов:</td>
									<td><? echo $diplom_num ?></td>
								</tr>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<hr class="divider-color">
	<footer>
		<div class="footer-bottom">
			<div class="grid-row clear-fix">
				<div class="copyright">QuickR<span></span> 2017 г. Разработчик: Декун Надежда, НМТ-443907.</div>
				<nav class="footer-nav">
					<ul class="clear-fix">
							<li>
								<a href="index.php">Поиск</a>
							</li>
							<li>
								<a href="news.php" >Новости</a>
							</li>
              <?php
                    if(isset($_SESSION["session_username"])){
              ?>
              <li>
                <a href="authorization.php">Личный кабинет</a>
              </li>
              <?php
              }else{
              ?>
							<li">
								<a>Войти</a>
							</li>
							<li>
								<a href="login.php">Зарегистрироваться</a>
							</li>
              <?php
                }
              ?>
						</ul>
				</nav>
			</div>
		</div>
	</footer>

</body>
</html>
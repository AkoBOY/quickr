<?php
session_start();
require_once("includes/connect.php");
require_once("includes/register.php");		
	/*Проверка авторизирован ли пользователь если да то отпровляем на страницу для авторизованных*/

    if(isset($_SESSION["session_username"])){
          echo "<META HTTP-EQUIV='REFRESH' CONTENT='0; URL=authorization.php'>";
        }

    /*Проверка полей авторизации*/

    if(isset($_POST["login"])){
      if(!empty($_POST['username']) && !empty($_POST['password'])) {
      $username=htmlspecialchars($_POST['username']);
      $password=htmlspecialchars($_POST['password']);
      $query_avto = mysql_query("SELECT * FROM `t_users` WHERE `email` ='$username' AND `password` ='$password'");
      $numrows=mysql_num_rows($query_avto);
    if($numrows!=0)
        {
        	/*Добавление в кеш*/
          $_SESSION['session_username'] =$username; 

          /* Перенаправление браузера */
          echo "<META HTTP-EQUIV='REFRESH' CONTENT='0;URL= authorization.php'> ";
  } 
      else {
              $message = "Неправильный пароль или логин";
            }
  } 
}
  ?>
<html>
<head>
    <title>QuickR-Отчет по НИИ</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
    <!-- style -->
    <link rel="shortcut icon" href="img/favicon.png">
    <link rel="stylesheet" href="fi/flaticon.css">
    <link rel="stylesheet" href="css/font-awesome.css">
    <link rel="stylesheet" href="css/main.css">
    <style>
        #menu-item-2:hover .login-block {
            display: block !important;
        }
    </style>

</head>
<body class="pc">
	<header class="only-color">
		<div class="sticky-wrapper">
			<div class="sticky-menu">
				<div class="grid-row clear-fix">
					<a href="index.php" class="logo">
						<img src="img/logo.png">
						<h1>QuickR</h1>
					</a>
					<nav class="main-nav">
						<i class="mobile_menu_switcher"></i>
						<ul class="clear-fix">
							<li id="menu-item-0">
								<a href="index.php">Поиск</a>
							</li>
							<li id="menu-item-3">
								<a href="news.php">Новости</a>
							</li>
							<li id="menu-item-2">
								<a>Войти</a>
								<div class="login-block">

									<h2>Авторизация</h2>
									<form class="login-form" method="post">
										<div class="form-group">
											<input type="text" class="login-input" placeholder="E-mail" name="username">
											<span class="input-icon">
												<i class="fa fa-user"></i>
											</span>
										</div>
										<div class="form-group">
											<input type="password" class="login-input" name = 'password' onblur="if(this.value=='')this.value='Пароль'" onfocus="if(this.value=='Пароль')this.value='' " placeholder="Пароль">
											<span class="input-icon">
												<i class="fa fa-lock"></i>
											</span>
										</div>
										<p class="small">
											<a href="">Забыли пароль?</a>
										</p>
										<button name = 'login' class="btn_log cws-button bt-color-5 border-radius">Войти</button>
										<?php
											echo $message;
										?>
									</form>
								</div>
							</li>
							<li id="menu-item-3">
								<a href="login.php" class="active">Зарегистрироваться</a>
							</li>
						</ul>
					</nav>
				</div>
			</div>
		</div>
	</header>
	<?php
		echo $message;
	?>
	<section>
		<div align="center" class="clear-fix">
			<div class="grid-col-6">
				<div class="banner-offer icon-right bg-color-4">
					<div class="banner-text">Личный кабинет</div>
					<div class="banner-icon">
						<i class="fa fa-user"></i>
					</div>
				</div>
			</div>
		</div>
	</section>


	<section style="margin-top:25px">
		<div class="tabs">
			<div class="block-tabs-btn clear-fix">
				<div class="tabs-btn" data-tabs-id="tabs1">Основные данные</div>
			</div>
			<div class="tabs-keeper">

				<div class="container-tabs active" data-tabs-id="cont-tabs1">
				<form method="post">
					<div class="container clear-fix">
						<table class="add_users">
							<tr>
								<td>
									<div class="form-row add_users ">
										<h4>Фамилия</h4>
										<input type="text" class="add_users" value="" placeholder="" name="lastname" required>
									</div>
								</td>
								<td>
									<div class="form-row add_users">
										<h4>Имя</h4>
										<input type="text" class="add_users" value="" placeholder="" name="firstname" required>
									</div>
								</td>
								<td>
									<div class="form-row add_users">
										<h4>Отчество*</h4>
										<input type="text" class="add_users" value="" placeholder="" name="secondname" >
									</div>
								</td>
							</tr>
							<tr>
								<td>
									<div class="form-row add_users">
										<h4>Дата рождения</h4>
										<input type="text" class="add_users" value="" placeholder="" name="date_birth" required>
									</div>
								</td>
								<td>
									<div class="form-row select-arrow course_finder">
										<h4>Пол</h4>
										<select name="select_gender" class="select_gender" required">
											<option value="0">Женщина</option>
											<option value="1">Мужчина</option>
										</select>
									</div>
								</td>
							</tr>
							<tr>
								<td>
									<div class="form-row-select select-arrow  course_finder" action="#" method="post">
										<h4>Должность</h4>
										<select name="select_post" class="select_teacher" required>
											<option value="0">Аспирант</option>
											<option value="1">Ассистент</option>
											<option value="2">Ведущий научный сотрудник</option>
											<option value="3">Главный научный сотрудник</option>
											<option value="4">Докторант</option>
											<option value="5">Доцент</option>
											<option value="6">Младший научный сотрудник</option>
											<option value="7">Научный сотрудник</option>
											<option value="8">Преподаватель</option>
											<option value="9">Профессор</option>
											<option value="10">Стажер</option>
											<option value="11">Старший научный сотрудник</option>
											<option value="12">Старший преподаватель</option>
											<option value="13">Студент</option>
										</select>
									</div>
								</td>
								<td>
									<div class="form-row-select select-arrow course_finder">
										<h4>Ученое звание</h4>
										<select name="select_rank" class="select_teacher">
											<option value="0"></option>
											<option value="1">Действительный член (академик) Академии наук</option>
											<option value="2">Доцент</option>
											<option value="3">Профессор</option>
											<option value="4">Член-корресподент (член-корр.) Академии наук</option>
										</select>
									</div>
								</td>
								<td>
									<div class="form-row-select select-arrow course_finder">
										<h4>Ученая степень</h4>
										<select name="select_degree" class="select_teacher">
											<option value="0"></option>
											<option value="1">Доктор наук</option>
											<option value="2">Кандидат наук</option>
										</select>
									</div>
								</td>
							</tr>
							<tr>
								<td>
									<div class="form-row add_users">
										<h4>Контактный телефон</h4>
										<input type="text" class="add_users" value="" name="telephone" id="telephone" required>
									</div>
								</td>
								<td>
									<div class="form-row add_users">
										<h4>E-mail</h4>
										<input type="text" class="add_users" value="" placeholder="" name="email" id="e-mail" required>
									</div>
								</td>
								<td>
									<div class="form-row add_users" id="account_password_user">
										<h4>Пароль</h4>
										<input type="password" class="add_users" name="account_password" id="account_password" value="" required/>
									</div>
								</td>
							</tr>

						</table>
						<div class="treatment">
							<div class="checkbox_treatment">
								<input type="hidden" name="obrabot" value="0" />
								<input type="checkbox" id="checkbox8" name="obrabot" value="1" />
								<label for="checkbox8">&nbsp;Я согласен(на) на обработку данных</label>
							</div>
						</div>
						<div class="button_tabs">
							<button type="submit" name="save_user" class="cws-button bt-color-4 border-radius alt">Сохранить</button>
							<button type="submit" name="edit_user" class="cws-button bt-color-4 border-radius alt">Редактировать</button>
						</div>
					</div>
					</form>
				</div>
				<?php
						/*Проверка нажатия на кнопку сохронить*/
						if (isset($_POST['save_user'])) {
							/*Проверка разрешения на обработку данных*/
							if ($_POST['obrabot'] != 0) {
								reg();
							}
							else{
								echo "Нужно согласие на оброботку данных";
							}
						}
				?>
				
				</div>
			</div>
			</section>
	</div>





	<footer>
		<div class="footer-bottom">
			<div class="grid-row clear-fix">
				<div class="copyright">QuickR<span></span> 2017 г. Разработчик: Декун Надежда, НМТ-443907.</div>
				<nav class="footer-nav">
					<ul class="clear-fix">
						<li>
							<a href="index.html">Поиск</a>
						</li>
						<li>
							<a href="news.html">Новости</a>
						</li>
						<li>
							<a href="login.html">Зарегистрироваться</a>
						</li>
						<li>
							<a href="login.html">Войти</a>
						</li>
					</ul>
				</nav>
			</div>
		</div>
	</footer>
	<script src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/jquery.validate.min.js"></script>
	<script src="js/jquery.form.min.js"></script>
	<script src="js/TweenMax.min.js"></script>
	<script src="js/main.js"></script>
	<script>
		function toggle(el) {
			el.style.display = (el.style.display == 'none') ? 'block' : 'none';
		}
	</script>
	<script type="text/javascript">
		if (document.getElementById('jh45').checked) {
			alet("Нужно согласие на оброботку данных");
		}

	</script>
	<script type="text/javascript">
		function(){     
    if($("#checkbox8").attr("checked") != 'checked') { 
        window.alert('Дайте свое согласие на обработку данных!');
        $("#checkbox8").css('border', '1px solid red');
        return false;
    }
    return true;
}
	</script>


</body>
</html>
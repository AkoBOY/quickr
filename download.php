<?php
session_start();

require 'vendor/autoload.php';
require('includes/publication.php');


use Dompdf\Dompdf;
use Dompdf\Options;

$options = new Options();
$options->set('defaultFont', 'DejaVu Sans');
$result = renderPdf(publicationData());


$dompdf = new Dompdf($options);
$dompdf->loadHtml($result);
$dompdf->setPaper('A4', 'portrait');
$dompdf->render();
$dompdf->stream();

/*
$html = publication();
$result = '<html><head><style>body { font-family: DejaVu Sans }</style><body>';
$result .= $html;
$result .= '</body></head></html>';
 */